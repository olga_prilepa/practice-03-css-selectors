# Практика #3 CSS-селекторы
* Презентация с детальным описанием заданий https://slides.com/frontschool-nsk/css#/

## Задание #1
* Сверстать виджет рейтинга в файле task-1.html
* Стилизация SVG https://developer.mozilla.org/ru/docs/Web/SVG/Tutorial/Fills_and_Strokes


## Задание #2
* Сверстать макет страницы в файле task-2.html
* Макеты лежат в папке layout
* Палитра цветов https://guides.kontur.ru/resources/colors/
* Тогл https://guides.kontur.ru/components/toggle/

